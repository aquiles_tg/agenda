﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using DevExpress.XtraScheduler;
using System.Data.OleDb;
using System.IO;

namespace Agenda
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.schedulerStorage1.AppointmentsInserted += OnApptChangedInsertedDeleted;
            this.schedulerStorage1.AppointmentsChanged += OnApptChangedInsertedDeleted;
            this.schedulerStorage1.AppointmentsDeleted += OnApptChangedInsertedDeleted;
            //this.schedulerControl1.InitNewAppointment += schedulerControl1_InitNewAppointment;
            //this.schedulerControl1.InitAppointmentDisplayText += schedulerControl1_InitAppointmentDisplayText;
            this.schedulerControl1.ActiveViewType = SchedulerViewType.Month;
        }

        DataSet DXSchedulerDataset;
        OleDbDataAdapter AppointmentDataAdapter;
        OleDbDataAdapter ResourceDataAdapter;
        OleDbConnection DXSchedulerConn;

        private void Form1_Load(object sender, EventArgs e)
        {
            string rutaInstalacion = string.Empty;
            string cadena = string.Empty;

            try
            {
                // Modify this string if required to connect to your database.
                //cadena = ConfigurationManager.ConnectionStrings["CadenaAgenda"].ConnectionString;
                rutaInstalacion = Application.StartupPath + "\\Agenda.mdb";

                if (System.IO.File.Exists(rutaInstalacion))
                    cadena = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + rutaInstalacion;
                else
                {
                    MessageBox.Show("No se pudo encontrar la base de datos", "Mi Agenda", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Application.Exit();
                }

            }
            catch
            {
                MessageBox.Show("No se pudo encontrar la base de datos", "Mi Agenda", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }

            this.schedulerStorage1.Appointments.ResourceSharing = true;
            this.schedulerControl1.GroupType = SchedulerGroupType.Resource;
            this.schedulerControl1.Start = DateTime.Today;

            DXSchedulerDataset = new DataSet();
            string selectAppointments = "SELECT * FROM Appointments";
            string selectResources = "SELECT * FROM Resources";

            DXSchedulerConn = new OleDbConnection(cadena);

            try
            {
                DXSchedulerConn.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("No se pudo conectar a la base de datos", "Mi Agenda", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }

            AppointmentDataAdapter = new OleDbDataAdapter(selectAppointments, DXSchedulerConn);
            // Subscribe to RowUpdated event to retrieve identity value for an inserted row.
            AppointmentDataAdapter.RowUpdated += new OleDbRowUpdatedEventHandler(AppointmentDataAdapter_RowUpdated);
            AppointmentDataAdapter.Fill(DXSchedulerDataset, "Appointments");

            ResourceDataAdapter = new OleDbDataAdapter(selectResources, DXSchedulerConn);
            ResourceDataAdapter.Fill(DXSchedulerDataset, "Resources");

            // Specify mappings.
            MapAppointmentData();
            MapResourceData();

            // Generate commands using CommandBuilder.  
            OleDbCommandBuilder cmdBuilder = new OleDbCommandBuilder(AppointmentDataAdapter);
            AppointmentDataAdapter.InsertCommand = cmdBuilder.GetInsertCommand();
            AppointmentDataAdapter.DeleteCommand = cmdBuilder.GetDeleteCommand();
            AppointmentDataAdapter.UpdateCommand = cmdBuilder.GetUpdateCommand();

            DXSchedulerConn.Close();

            this.schedulerStorage1.Appointments.DataSource = DXSchedulerDataset;
            this.schedulerStorage1.Appointments.DataMember = "Appointments";
            this.schedulerStorage1.Resources.DataSource = DXSchedulerDataset;
            this.schedulerStorage1.Resources.DataMember = "Resources";
        }

        private void MapAppointmentData()
        {
            this.schedulerStorage1.Appointments.Mappings.AllDay = "AllDay";
            this.schedulerStorage1.Appointments.Mappings.Description = "Description";
            // Required mapping.
            this.schedulerStorage1.Appointments.Mappings.End = "EndDate";
            this.schedulerStorage1.Appointments.Mappings.Label = "Label";
            this.schedulerStorage1.Appointments.Mappings.Location = "Location";
            this.schedulerStorage1.Appointments.Mappings.RecurrenceInfo = "RecurrenceInfo";
            this.schedulerStorage1.Appointments.Mappings.ReminderInfo = "ReminderInfo";
            // Required mapping.
            this.schedulerStorage1.Appointments.Mappings.Start = "StartDate";
            this.schedulerStorage1.Appointments.Mappings.Status = "Status";
            this.schedulerStorage1.Appointments.Mappings.Subject = "Subject";
            this.schedulerStorage1.Appointments.Mappings.Type = "Type";
            this.schedulerStorage1.Appointments.Mappings.ResourceId = "ResourceIDs";
            this.schedulerStorage1.Appointments.CustomFieldMappings.Add(new AppointmentCustomFieldMapping("Cliente", "CustomField1"));
            this.schedulerStorage1.Appointments.CustomFieldMappings.Add(new AppointmentCustomFieldMapping("Hotel", "CustomField2"));
        }

        private void MapResourceData()
        {
            this.schedulerStorage1.Resources.Mappings.Id = "ResourceID";
            this.schedulerStorage1.Resources.Mappings.Caption = "ResourceName";
        }

        // Retrieve identity value for an inserted appointment.
        void AppointmentDataAdapter_RowUpdated(object sender, OleDbRowUpdatedEventArgs e)
        {
            if (e.Status == UpdateStatus.Continue && e.StatementType == StatementType.Insert)
            {
                int id = 0;
                using (OleDbCommand cmd = new OleDbCommand("select MAX(UniqueID) from Appointments", DXSchedulerConn))
                {
                    id = Convert.ToInt32(cmd.ExecuteScalar());
                }
                e.Row["UniqueID"] = id;

                //hoteles.UpdEstancia(id, Convert.ToInt32(cmbcliente.EditValue), Convert.ToInt32(cmbhotel.EditValue));
            }
            else if (e.Status == UpdateStatus.Continue && e.StatementType == StatementType.Update)
            {
                int id = 0;
                id = (int)e.Row["UniqueID"];

                //hoteles.UpdEstancia(id, Convert.ToInt32(cmbcliente.EditValue), Convert.ToInt32(cmbhotel.EditValue));
            }
        }

        // Store modified data in the database
        private void OnApptChangedInsertedDeleted(object sender, PersistentObjectsEventArgs e)
        {
            try
            {
                AppointmentDataAdapter.Update(DXSchedulerDataset.Tables["Appointments"]);
                DXSchedulerDataset.AcceptChanges();
            }
            catch (Exception ex)
            {
            }
        }

        private void schedulerStorage1_AppointmentDeleting(object sender, PersistentObjectCancelEventArgs e)
        {
            if (MessageBox.Show("¿Desea eliminar esta actividad?", "Mi Agenda", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void schedulerStorage1_AppointmentInserting(object sender, PersistentObjectCancelEventArgs e)
        {

        }
    }
}
